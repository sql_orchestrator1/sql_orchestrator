# Node + Express

1. Config your database in file .env
```
MYSQL_HOST=  // ex: localhost
MYSQL_USER=  // ex: root
MYSQL_PASSWORD=  //ex:
MYSQL_DATABASE=  //ex: sql_orchestrator
MYSQL_PORT=  //ex: 3306
MYSQL_CONNECTION_LIMIT= //ex: 10
```

2. Start the server
```
npm run dev
```
If you do not change the port in the file .env, the server will run on port 8000 

3. Run list test case

```bash
# add 1 user (NO ERROR, NO REVERT)
curl -X POST http://localhost:8000/user/addMultiple
   -H 'Content-Type: application/json'
   -d '{"type":"my_login","cmd_chain":[{"type":"sql_safe","cmd":"INSERT INTO Users (Uid, Username, City, Friend) VALUES (1, 'tom', 'France', NULL);"}]}'

return_object = {
	status: "ok", # status 200
	dbState: ["(1, 'tom', 'France', NULL)"]
}

# add same user (RETURN ERROR CODE, REVERT CHANGE)
curl -X POST http://localhost:8000/user/addMultiple
   -H 'Content-Type: application/json'
   -d '{"type":"my_login","cmd_chain":[{"type":"sql_safe","cmd":"INSERT INTO Users (Uid, Username, City, Friend) VALUES (2, 'frog', 'France', NULL);"},{"type":"sql_safe","cmd":"INSERT INTO Users (Uid, Username, City, Friend) VALUES (1, 'sammy', 'France', NULL);"}]}'

return_object = {
	status: "error", # status 400
	dbState: ["(1, 'tom', 'France', NULL)"]
}

# add 2 users synchronously (NO ERROR, NO REVERT)
curl -X POST http://localhost:8000/user/addMultiple
   -H 'Content-Type: application/json'
   -d '{"type":"my_login","cmd_chain":[{"type":"sql_safe","cmd":"INSERT INTO Users (Uid, Username, City, Friend) VALUES (2, 'frog', 'France', NULL);"},{"type":"sql_safe","cmd":"INSERT INTO Users (Uid, Username, City, Friend) VALUES (3, 'sam', 'England', 1);"}]}'

return_object = {
	status: "ok", # status 200
	dbState: ["(1, 'tom', 'France', NULL)", "(2, 'frog', 'France', NULL)", "(3, 'sam', 'England', 1)"]
}

# Invalid Foreign Key error thrown by DB (RETURN ERROR CODE, REVERT CHANGE)
curl -X POST http://localhost:8000/user/addMultiple
   -H 'Content-Type: application/json'
   -d '{"type":"my_login","cmd_chain":[{"type":"sql_safe","cmd":"INSERT INTO Users (Uid, Username, City, Friend) VALUES (4, 'croak', 'Malaysia', NULL);"},{"type":"sql_safe","cmd":"INSERT INTO Users (Uid, Username, City, Friend) VALUES (5, 'ding', 'Finland', 100);"}]}'

return_object = {
	status: "error", # status 400
	dbState: ["(1, 'tom', 'France', NULL)", "(2, 'frog', 'France', NULL)", "(3, 'sam', 'England', 1)"]
}
```

Note: Remember to use "http" instead of "https", because I don't configure SSL on the server.