const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const dotenv = require("dotenv");
const mysql = require('mysql');
dotenv.config();

const { PORT, MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT, MYSQL_CONNECTION_LIMIT } = process.env;
const port = PORT || 8000;
const REQ_TYPE = {
    LOGIN: 'my_login'
}
const RES_STATUS = {
    OK: 'ok',
    ERROR: 'error'
}
const RES_STATUS_CODE = {
    [RES_STATUS.OK]: 200,
    [RES_STATUS.ERROR]: 400
}

const configMySql = {
    host: MYSQL_HOST,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DATABASE,
    connectionLimit: MYSQL_CONNECTION_LIMIT,
    port: MYSQL_PORT,
    multipleStatements: true
};
const connection = mysql.createConnection(configMySql);

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);

app.get("/", (request, response) => {
    response.json({ info: "Server is running" });
});

const querySql = (query) => new Promise((resolve, reject) => {
    connection.beginTransaction(error => {
        if (error) { reject(error); }
        connection.query(query, function(error, result) {
            if (error) {
                return connection.rollback(function() {
                    reject( error);
                });
            }
            connection.commit(function(err) {
                if (err) {
                    return connection.rollback(function() {
                        reject(err);
                    });
                }
                resolve(result);
            });
        });
    })
})

app.post("/user/addMultiple", async(req, res) => {
    const { type, cmd_chain } = req.body;
    if (!type || !cmd_chain || type != REQ_TYPE.LOGIN || !Array.isArray(cmd_chain))
        return res.status(400).send({ status: "error", msg: "Value is invalid" });

    const query = cmd_chain.map(item => item.cmd).join('').toLocaleLowerCase();
    let status = RES_STATUS.ERROR;
    try {
        await querySql(query);
        status = RES_STATUS.OK;
    } catch (err) {
        console.log(err)
    }
    const dbState = await querySql('select * from users')

    return res.status(RES_STATUS_CODE[status]).send({
        status,
        dbState
    });
});

app.use((req, res, next) => {
    res.status(404).send('Not found');
});

connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('MySQL connected as id ' + connection.threadId);
    app.listen(port, () => {
        console.log(`Listening to port ${port}.`);
    });
});